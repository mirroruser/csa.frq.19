import java.util.ArrayList;

public class Delimiters {
    private String openDel;
    private String closeDel;

    public Delimiters(String open, String close) {
        this.openDel = open;
        this.closeDel = close;
    }

    public ArrayList<String> getDelimiterList(String[] tokens) {
        ArrayList<String> out = new ArrayList<>();

        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (token.equals(this.openDel) || token.equals(this.closeDel)) {
                out.add(token);
            }
        }
        return out;
    }

    public boolean isBalanced(ArrayList<String> delimiters) {
        int openDelimiters = 0;
        int closeDelimiters = 0;

        for (int i = 0; i < delimiters.size(); i++) {
            String delimiter = delimiters.get(i);
            if (delimiter.equals(this.openDel)) {
                openDelimiters = openDelimiters + 1;
            }

            if (delimiter.equals(this.closeDel)) {
                closeDelimiters = closeDelimiters + 1;
            }

            if (closeDelimiters > openDelimiters) {
                return false;
            }
        }

        return openDelimiters == closeDelimiters;
    }

    public static void main(String[] args) {
        Delimiters d = new Delimiters("(", ")");
        String[] strs = { "(", "x + y", ")", "* 5" };
        ArrayList<String> strsal = d.getDelimiterList(strs);
        System.out.println(strsal.toString());

        Delimiters dd = new Delimiters("<q>", "</q>");
        String[] dstrs = { "<q>", "yy", "</q>", "zz", "</q>" };
        ArrayList<String> dstrsal = dd.getDelimiterList(dstrs);
        System.out.println(dstrsal.toString());

        Delimiters ddd = new Delimiters("<sup>", "</sup>");
        String[] ddstrs = { "<sup>", "<sup>", "</sup>", "<sup>", "</sup>", "</sup>" };
        System.out.println(ddd.isBalanced(ddd.getDelimiterList(ddstrs)));

        Delimiters dddd = new Delimiters("<sup>", "</sup>");
        String[] dddstrs = { "<sup>", "</sup>", "</sup>", "<sup>" };
        System.out.println(dddd.isBalanced(dddd.getDelimiterList(dddstrs)));

        Delimiters ddddd = new Delimiters("<sup>", "</sup>");
        String[] ddddstrs = { "</sup>" };
        System.out.println(ddddd.isBalanced(ddddd.getDelimiterList(ddddstrs)));

        Delimiters dddddd = new Delimiters("<sup>", "</sup>");
        String[] dddddstrs = { "<sup>", "<sup>", "</sup>" };
        System.out.println(dddddd.isBalanced(dddddd.getDelimiterList(dddddstrs)));
    }
}
