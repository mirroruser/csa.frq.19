public class LightBoard {
    private boolean[][] lights;

    public LightBoard(int numRows, int numCols) {
        this.lights = new boolean[numRows][numCols];

        for (int r = 0; r < numRows; r++) {
            for (int c = 0; c < numCols; c++) {
                boolean on = Math.random() < 0.4;
                this.lights[r][c] = on;
            }
        }
    }

    public boolean evaluateLight(int row, int col) {
        boolean status = this.lights[row][col];
        int lightsOnInCol = 0;

        for (int r = 0; r < this.lights.length; r++) {
            if (this.lights[r][col]) {
                lightsOnInCol = lightsOnInCol + 1;
            }
        }

        if (status && lightsOnInCol % 2 == 0) {
            return false;
        }

        if (!status && lightsOnInCol % 3 == 0) {
            return true;
        }

        return status;
    }

    public void setLights(boolean[][] lights) {
        this.lights = lights;
    }

    public static void main(String[] args) {
        boolean[][] lights = {
                { true, true, false, true, true },
                { true, false, false, true, false },
                { true, false, false, true, true },
                { true, false, false, false, true },
                { true, false, false, false, true },
                { true, true, false, true, true },
                { false, false, false, false, false }
        };

        LightBoard sim = new LightBoard(7, 5);
        sim.setLights(lights);
        System.out.println(sim.evaluateLight(0, 3));
        System.out.println(sim.evaluateLight(6, 0));
        System.out.println(sim.evaluateLight(4, 1));
        System.out.println(sim.evaluateLight(5, 4));
    }
}
