public class StepTracker {
    private int minSteps;
    private int activeDays;
    private double totalSteps;
    private int totalDays;

    public StepTracker(int minSteps) {
        this.minSteps = minSteps;
        this.activeDays = 0;
        this.totalSteps = 0;
        this.totalDays = 0;
    }

    public int activeDays() {
        return this.activeDays;
    }

    public double averageSteps() {
        if (this.totalDays == 0) {
            return 0;
        }

        return this.totalSteps / this.totalDays;
    }

    public void addDailySteps(int steps) {
        if (steps >= this.minSteps) {
            this.activeDays = this.activeDays + 1;
        }
        this.totalSteps = this.totalSteps + steps;
        this.totalDays = this.totalDays + 1;
    }

    public static void main(String[] args) {
        StepTracker tr = new StepTracker(10000);
        System.out.println(tr.activeDays());
        System.out.println(tr.averageSteps());
        tr.addDailySteps(9000);
        tr.addDailySteps(5000);

        System.out.println(tr.activeDays());
        System.out.println(tr.averageSteps());
        tr.addDailySteps(13000);

        System.out.println(tr.activeDays());
        System.out.println(tr.averageSteps());
        tr.addDailySteps(23000);
        tr.addDailySteps(1111);
        tr.activeDays();
        tr.averageSteps();
    }
}
